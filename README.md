# README #

This repository explains the use of crypt encryption mechanism in iOS application. 

### What is this repository for? ###

* This provides a quick reference on how to encrypt data using common-crypto mechanism in iOS application
* 1.0.0
* [Learn Markdown](https://github.com/rvndios/CommonCrypt-Swift)

### How do I get set up? ###

* Download the project from the Repository
* Do pod install (This may require third party dependancies like cocoa-pods)
* For Quick reference on Integration, Please refer the above git-hub link.
* Run and Build the project

### Who do I talk to? ###

* Repo owner or admin